import numpy as np


class Point2d:
    
    def __init__(self, x, y):
        self.x = x
        self.y = y




class Point3d:
    
    def __init__(self, x, y, z):
        self.x = x
        self.y = y
        self.z = z
        
    def print(self):
        print("(" + str(round(self.x,1)) + ", " \
                + str(round(self.y,1)) + ", " \
                + str(round(self.z,1)) + ")")
    
    
        


class CornerPoint(Point3d): 
    
    def __init__(self, x, y, z):
        super().__init__(x, y, z)
        self.shared = False
        self.polygons = []  # unclear if this should be a list of just polygons or something more/else
        self.index = -1
        
    def updateIndex(self,i):
        self.index = i
        



# stores point data as a list of Point3d objects
class PointCloud:
    
    # input: pointList :: [Point3d]
    def __init__(self, pointList):
        self.points = pointList
        self.current = -1
        self.high = self.size()
        
        
    def __iter__(self):
        return self    
    
    # output:: Point3d
    def __next__(self):
        self.current += 1
        if self.current < self.high:
            return self.points[self.current]
        raise StopIteration

    
    def size(self):
        return len(self.points)
    
    # output :: [double]
    def xs(self):
        return [p.x for p in self.points]
    
    # output :: [double]    
    def ys(self):
        return [p.y for p in self.points]
    
    # output :: [double]
    def zs(self):
        return [p.z for p in self.points]
    
    
    def mean(self):
        return [sum(self.xs())/self.size(), sum(self.ys())/self.size(), \
                sum(self.zs())/self.size()]
        
        
    def add(self, point: Point3d or list):
        if type(point) ==Point3d:
            self.coordinates.append(point)
        elif type(point) == list:
            self.coordinates.append(Point3d(point[0], point[1], point[2]))
    
        
        
        
class Polygon3d:
    
    def __init__(self, footprint, pointCloud):
        self.footprint = footprint # :: [Point2d]
        self.pointCloud = pointCloud # the points contained inside the polygon
        self.cornerPoints = [] 
        self.planeEq = []
    
    
    def size(self):
        return self.pointCloud.size()
    
    
    
    
    
    