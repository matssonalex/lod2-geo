# -*- coding: utf-8 -*-
"""
Created on Wed Mar 24 11:14:32 2021

@author: Alexander Samuelsson
"""
import numpy as np
import json
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D


##Some useful functions when doing manual segmentations



with open('./CityModel.json') as f:
 data = json.load(f)
    
 #Plots heightmap of building
def plotroofmap(n):
    roofpoints = data['Buildings'][n]['RoofPoints']
    
    x = [ dic['x'] for dic in roofpoints ]
    y = [ dic['y'] for dic in roofpoints ]
    z = [ dic['z'] for dic in roofpoints ]
    
    
    plt.scatter(x, y, c=z)

#Plots footprint of building
def plotfootprint(n):
 
    footprint = data['Buildings'][n]['Footprint']
    print(footprint)
    x = [ dic['x'] for dic in footprint ]
    y = [ dic['y'] for dic in footprint ]
    x.insert(len(x), x[0])
    y.insert(len(y), y[0])
    
    plt.plot(x,y)
    
#If you want to plot the roof in 3d
def plotroof(n):

    fig = plt.figure()
    ax = Axes3D(fig)
    
    roofpoints = data['Buildings'][n]['RoofPoints']
    
    x = [ dic['x'] for dic in roofpoints ]
    y = [ dic['y'] for dic in roofpoints ]
    z = [ dic['z'] for dic in roofpoints ]
    
    
    ax.scatter(x,y,z, c='blue')
    
#plots all footprints of buildings in citymodel.json, incase you want to find a building to segment
def plotfootprints():
    
    for n in range(630):
        footprint = data['Buildings'][n]['Footprint']
        
        x = [ dic['x'] for dic in footprint ]
        y = [ dic['y'] for dic in footprint ]
        x.insert(len(x), x[0])
        y.insert(len(y), y[0])
        
        plt.plot(x,y)
      
        #if n == 624:
        plt.text(x[0],y[0],"{}".format(n))

n=279
plotroofmap(n)
plotfootprint(n)
# plotroof(n)
# plotfootprints()
plt.show()