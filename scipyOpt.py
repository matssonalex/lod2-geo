# -*- coding: utf-8 -*-
"""
Created on Fri Apr  2 15:31:36 2021

@author: Alexander Samuelsson
"""
from scipy.optimize import linprog
import numpy as np

def opt(polygons3d, vertices):
    #Formulate a linear optimization problem and use SciPy to solve it
    #Vertices have a unique index that is used to find them
    #Scipi uses arrays to model objective function and constraints, the code creates the correct arrays

    n = len(vertices)
    
    #Upperbound for decision variables, i.e how much we can move the cornerpoints up or down
    eps=1
    
            
    
    ##Create constraints. For each polygon choose the points that represent the plane 
    #and create constraints for the other points according to goebbels
    A=[]
    b=[]
    
    for pol in polygons3d:
        planePoints=choosePlanePoints(pol)
        
        for cp in pol.cornerPoints:
            if cp not in planePoints:
                a, b_row_leq, b_row_geq = createConstraint(cp,planePoints,pol, n)
                A.append(a)
                b.append(b_row_leq)
                #Add greater than constraint
                A.append([-1*x for x in a])
                b.append(b_row_geq)
                
    #Create objectiveFunction
    c = createObj(vertices)
    bounds = (0,eps)
    
    #Note: default method is interiorpoint, in general simplex seems to perform better
    res = linprog(c, A_ub=A, b_ub=b, bounds=bounds, method='revised simplex')
 
    h = res['x']
    
    #Loop through all corners and change their heigths according to the result
    for idx, v in enumerate(vertices):
        v.z+=h[idx] 
        v.z-=h[idx+1]

    return h
    
    
    
#input : list of all vertices in our roof :: [CornerPoint]
#Our objective function is just the sum of all decision variables i.e [1,1,1....]
def createObj(vertices):
        c = []
        for v in vertices:
            c.append(1.0)
            c.append(1.0)
        return c
    
    
    
#Calculate delta for a given plane. mu changes tolerance for how flat planes can be
#input :: Polygon3d
def createDelta(pol):
    mu = 0
    vz = pol.planeEq[2]
    # print("VZ")
    # print(vz)
    
    delta = mu + (np.sqrt(1-vz**2)/vz)*np.sqrt(2)*mu
    #print(delta)
    return delta



#Choose which corners that represent the plane for a given polygon
#For now we just take the first corners in the list. This works fine for few corners but can be improved
#input :: Polygon3d
def choosePlanePoints(pol):
    planePoints = []
    planePoints.append(pol.cornerPoints[0]) #p_ku
    planePoints.append(pol.cornerPoints[1]) #p_kv
    planePoints.append(pol.cornerPoints[2]) #p_kw
    
    return planePoints



#Compute constants rkj and skj with cramers rule
#input point :: CornerPoint, planePoints :: [CornerPoints]
def computeConstants(point,planePoints):
    #Cramers rule
    j=point
    u=planePoints[0]
    v=planePoints[1]
    w=planePoints[2]
   
    
    #We want rkj and skj so that pkj=pkv+rkj(pku-pkv)+skj(pkw-pkv) according to the paper
    #We compute the constants with cramers rule
    a=(u.x-v.x)
    b=(w.x-v.x)
    c=(u.y-v.y)
    d=(w.y-v.y)
    e=j.x-v.x
    f=j.y-v.y

    rkj=(e*d-b*f)/(a*d-b*c)
    skj=(a*f-e*c)/(a*d-b*c) 

    return rkj,skj



#Creates a constraint for a CornerPoint so that it is approximately planar in the plane represented  by planePoints
#A constraint is represented by an array in SciPy, we create the array a and fill it with values at the right indexes
#input : point :: CornerPoint, planePoints :: [CornerPoints], pol :: Polygon3d, n :: n.o decision variables
def createConstraint(point,planePoints,pol, n):
    
    #Create array with length = n.o decision variables
    a=[0]*n*2
    
    
    pkj=point.index
    pku=planePoints[0].index
    pkv=planePoints[1].index 
    pkw=planePoints[2].index
    rkj, skj = computeConstants(point,planePoints)
    
    
    #Add -h1(p_kj)+h2(p_kj)
    a[pkj*2]=-1.0
    a[pkj*2+1]=1.0
    
    #Add (1-rkj-skj)(h1(pkv)-h2(pkv))
    a[pkv*2]=(1-rkj-skj)
    a[pkv*2+1]=-(1-rkj-skj)
    
    #Add rkj(h1(pku)-h2(pku))
    a[pku*2]=rkj
    a[pku*2+1]=-rkj
    
    #Add skj(h1(pkw)-h2(pkw))
    a[pkw*2]=skj
    a[pkw*2+1]=-skj
    
    #Calculate constant ckj
    ckj = -point.z+(1-rkj-skj)*planePoints[1].z+rkj*planePoints[0].z+skj*planePoints[2].z 

    delta = createDelta(pol)
    
    #Move constant to other side of inequality
    b_row_leq=-ckj+delta 
    b_row_geq=ckj+delta
    
    return a, b_row_leq, b_row_geq
    
