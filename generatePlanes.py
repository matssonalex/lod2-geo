import numpy as np
from geometry import Point2d
from geometry import Point3d
from geometry import Polygon3d
from geometry import PointCloud
from geometry import CornerPoint
from scipy.optimize import minimize


#A collections of functions that are used when generating roofplanes

# check if a point is inside of a polygon
# input: p :: Point3d
#  polygon = [[x0,y0], [x1,y1], ..., [xn,yn]] but this should probably be changed to Polygon3d or Polygon2d if that's implemented
# output: True if point is inside of polygon, else False
def QuadrantAngle2D_point_poly(p, polygon):
    # compute angle to first vertex
    q0 = polygon[0]
    v0 = QuadrantAngle_point_point(q0, p)

    # sum up total angle
    totalAngle = 0
    for i in range(1, len(polygon)+1): 
        q1 = polygon[i % len(polygon)]
        v1 = QuadrantAngle_point_point(q1, p)
        dv = v1 - v0

        if dv == 3:
            dv = -1
        elif dv == -3:
            dv = 1
        elif dv == 2 or dv == -2:
            xx = q1.x - ((q1.y - p.y) * ((q0.x - q1.x) / (q0.y - q1.y)))
            if xx > p.x:
                dv = -dv

        totalAngle += dv
        q0 = q1
        v0 = v1

    return totalAngle != 0


# helper method for QuadrantAngle2D_point_poly
def QuadrantAngle_point_point(p, q):
    if p.x > q.x:
        if p.y > q.y:
            return 0
        else:
            return 3
    else:
        if p.y > q.y:
            return 1
        else:
            return 2



# filter out the points that are not inside the given polygon
# input: polygon = [[x0,y0], [x1,y1], ..., [xn,yn]] but this should probably be changed to Polygon3d or Polygon2d if that's implemented
#         every :: PointCloud
# output: points_inside :: pointCloud
def pointsInside(polygon, all_of_them):
    goodPoints = [point for point in all_of_them.points if 
               QuadrantAngle2D_point_poly(point, polygon)]
    points_inside = PointCloud(goodPoints)
    return points_inside



# computes the coefficients of the equation of the plane generated from the 
# point data
# input: points_inside :: PointCloud
# output: eq :: [double]
def eqFromCloud(points_inside):
    conversion = [[p.x, p.y, p.z] for p in points_inside.points]
    matrix = np.array(conversion)
    meanPoint = points_inside.mean()
    A =  matrix - np.array(meanPoint)
    u, s, vh = np.linalg.svd(A.T)
    normal = u[:, 2] 
    #print("NORMAL")
    #print(normal)
    D = np.dot(normal,meanPoint)
    eq = np.append(normal, D)
    # a*x + b*y + c*z = D, where D = normal*middlepoint
    # eq = [a,b,c,D] = plane equation
    return eq



# Generates the z-coordinates for the corners of each polygon, based on the 
# plane equation for that polygon.
# input: polygon3dList :: [Polygon3d]
# output: nothing, the Polygon3d objects are just modified
def zFromEq(polygon3dList):
    for pol in polygon3dList:
        eq = pol.planeEq
        pol.cornerPoints = []
        for idx, p in enumerate(pol.footprint):
            # below is just the equation of the plane, with z isolated
            z = (eq[3] - eq[0]*p.x - eq[1]*p.y)/eq[2]
            pol.cornerPoints.append(CornerPoint(p.x, p.y, z))
            


# removes points that are more than *threshold* metres from the plane defined
# by polygon.planeEq
# input: polygon :: Polygon3d
#        threshold :: double or Int
def removeOutliers(polygon, threshold):
    a = polygon.planeEq[0]
    b = polygon.planeEq[1]
    c = polygon.planeEq[2]
    D = polygon.planeEq[3]
    okPoints = []
    for p in polygon.pointCloud.points:
        #d = the distance from the plane to the point
        d = (a*p.x + b*p.y + c*p.z - D)/np.sqrt(a**2 + b**2 + c**2)
        if abs(d) <= threshold:
            okPoints.append(p)
    polygon.pointCloud.points = okPoints

    
    
# Finds all corner points that should count as the same point, i.e, the 
# distance between two points is less than the tolerances tolXY and tolZ. 
# input: polygon3dlist :: [Polygon3d]
#                tolXY :: double or int
#                tolZ :: double or int
# output: occurrences :: [[dict]], is a nested list where 
# each inner list contains all points that should be the same point, along 
# with the "address" (polygon and cornerpoints[] index) for each corner point. 
def findDuplicates(polygon3dList, tolXY, tolZ):    
    occurrences = []
    # add a bogus point so that there's something to compare with: 
    startingPoint = {'pol': 0, 'idx': 0, 'point': Point3d(0,0,-1000)}
    occurrences.append([startingPoint])
    for i, pol in enumerate(polygon3dList):
        for j, p in enumerate(pol.cornerPoints):
            pointAddress = {'pol' : i, 'idx' : j, 'point' : p}
            added = False
            for ps in occurrences: # compare with the other points
                q = ps[0]['point']
                distXY = np.sqrt((p.x - q.x)**2 + (p.y - q.y)**2)
                if distXY <= tolXY:
                    if abs(p.z-q.z) <= tolZ:
                        ps.append(pointAddress)
                        added = True
            if added == False:
                occurrences.append([pointAddress])
    # remove the first "comparison" point:
    occurrences.remove([startingPoint])
    return occurrences
    

    
# replaces the points that should be the same with one common CornerPoint
# object, that has the average coordinates as x- y- and z-values. Also logs
# which polygons these points are part of.
def replaceDuplicates(polygon3dList, tolXY, tolZ):
    occurrences = findDuplicates(polygon3dList, tolXY, tolZ)
    for ps in occurrences: # ps :: [dict]
        if len(ps) > 1: # unique points are ignored
            x = np.mean([p['point'].x for p in ps])
            y = np.mean([p['point'].y for p in ps])
            z = np.mean([p['point'].z for p in ps]) #+ np.random.normal(scale=0.5)
            sharedPoint = CornerPoint(x, y, z)
            sharedPoint.shared = True
            for p in ps: # for every polygon that this point is part of:
                pol = polygon3dList[p['pol']]
                sharedPoint.polygons.append(pol)
                pol.cornerPoints[p['idx']] = sharedPoint
    
    
    
    
    
    
