# -*- coding: utf-8 -*-
"""
Created on Mon Feb 15 09:33:21 2021

@author: Alexander Samuelsson
"""

import pyproj
import webbrowser
import json
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt

with open('./CityModelold.json') as f:
 data = json.load(f)

#Plots the pointcloud for a building and looks up that building in Google Maps
def plotroof(n):

    fig = plt.figure()
    ax = Axes3D(fig)
    
    roofpoints = data['Buildings'][n]['RoofPoints']
    
    x = [ dic['x'] for dic in roofpoints ]
    y = [ dic['y'] for dic in roofpoints ]
    z = [ dic['z'] for dic in roofpoints ]
    
    
    ax.scatter(x, y, z)
    
    
    proj = pyproj.Transformer.from_crs(3006, 4326, always_xy=True)

    x1, y1 = (323100+x[0], 6407490+y[0])
    x2, y2 = proj.transform(x1, y1)
    
    webbrowser.open('https://www.google.se/maps/@{},{},46m/data=!3m1!1e3'.format(y2,x2), 1)
    
plotroof(317)
#289 verkar ha många outliers
    