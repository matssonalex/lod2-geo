# -*- coding: utf-8 -*-
"""
Created on Sat May  1 15:14:26 2021

@author: Alexander Samuelsson
"""

# -*- coding: utf-8 -*-
"""
Created on Mon Mar 29 10:46:34 2021

@author: Alexander Samuelsson
"""

import numpy as np
import json
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d.art3d import Poly3DCollection
import timeit
from geometry import Point2d
from geometry import Point3d
from geometry import Polygon3d
from geometry import PointCloud

import manualSegmentations as man_made
import generatePlanes as generate
import scipyOpt as optimization

filePath = "./CityModel.json"
f = open(filePath)
data = json.load(f)

def read_data(building):
    roofpoints = data['Buildings'][building]['RoofPoints'] # all roof points
    pointList = [Point3d(roofpoints[i]['x'], roofpoints[i]['y'], \
                         roofpoints[i]['z']) for i in range(len(roofpoints))]
    point_cloud = PointCloud(pointList)  
    return point_cloud




# input:  polygons :: [Polygon3d]
#            fig :: Axes3DSubPlot (?)
def plotPoints(polygons, fig):
    colours = ['c', 'g', 'r', 'b', 'm', 'y']
    for idx, pol in enumerate(polygons):
        xCoords = list(pol.pointCloud.xs())
        yCoords = list(pol.pointCloud.ys())
        zCoords = list(pol.pointCloud.zs())
        fig.scatter(xCoords, yCoords, zCoords, color= colours[idx], alpha=0)

def plotPlanes(polygons, fig):
    colours = ['c', 'g', 'r', 'b', 'm', 'y']
    # convert from [CornerPoint] to Poly3DCollection and add to the plot:
    for idx, pol in enumerate(polygons):
        outline = [[[p.x, p.y, p.z] for p in pol.cornerPoints]]
        polygonPlane = Poly3DCollection(outline)
        polygonPlane.set_facecolor(colours[idx])
        polygonPlane.set_edgecolor('k')
        fig.add_collection3d(polygonPlane)   


#Calculates flatness of plane and how well it fits points
# input:  cornerPoints :: [cornerPoints]
def calculateFlatnessDistance(cornerPoints, pol):
    #Choose 3 first points and let them represent the plane
    planePoints = [cornerPoints[0],cornerPoints[1],cornerPoints[2]]
    
    p1 = [cornerPoints[0].x, cornerPoints[0].y, cornerPoints[0].z]
    v1 = [cornerPoints[1].x-cornerPoints[0].x,cornerPoints[1].y-cornerPoints[0].y, cornerPoints[1].z-cornerPoints[0].z]
    v2 = [cornerPoints[2].x-cornerPoints[0].x, cornerPoints[2].y-cornerPoints[0].y, cornerPoints[2].z-cornerPoints[0].z]
    
    normal = np.cross(v1,v2)
    normal = normal/sum(normal)
    
    D = -np.dot(normal,p1)
    
    planeEq = [normal[0], normal[1], normal[2], D]
    
    #Calculate flatness by calculating distance from point to plane for each corner not representing the plane
    flatness = 0
    for cp in cornerPoints:
        if cp not in planePoints:
            flatness += np.abs(planeEq[0]*cp.x+planeEq[1]*cp.y+planeEq[2]*cp.z+planeEq[3])/np.sqrt(planeEq[0]**2+planeEq[1]**2+planeEq[2]**2)
    
    #Calculate distance by calculating distance from each point in the point cloud to the plane
    dist = 0
    for p in pol.pointCloud.points:
        dist +=np.abs(planeEq[0]*p.x+planeEq[1]*p.y+planeEq[2]*p.z+planeEq[3])/np.sqrt(planeEq[0]**2+planeEq[1]**2+planeEq[2]**2)
        
    return flatness, dist/pol.size()
            
def filterPoints(polygon, thresholds):
    for threshold in thresholds:
        generate.removeOutliers(polygon, threshold)
        polygon.planeEq = generate.eqFromCloud(polygon.pointCloud)

        

#input : n :: number of building to generate, 

#The "main" function of the program. Input a building number to generate its roof
def roofPlanes(n):
    
    start = timeit.default_timer()
    ax = plt.subplot(111, projection=r"3d", autoscale_on= True) # create blank plot for first subplot
    #ax2 = plt.subplot(121, projection=r"3d", autoscale_on= True) # create blank plot for second subplot
    
    every_point = read_data(n) # get the point cloud
    footprint = man_made.footprints[n] # get the footprint for the building
    polygon3dList = []

    
    for polygon2d in footprint:   # footprint :: [[Point2d]]
        # pointCloud = all points that are contained inside the current polygon. 
        pointCloud = generate.pointsInside(polygon2d, every_point)
        pol3d = Polygon3d(polygon2d, pointCloud)
        pol3d.planeEq = generate.eqFromCloud(pol3d.pointCloud)

        # change list below for more/less aggressive outlier removal
        filterPoints(pol3d, [2, 1, 0.5])

        polygon3dList.append(pol3d)
        
    generate.zFromEq(polygon3dList)
    
    tolXY=0.1
    tolZ=2
    generate.replaceDuplicates(polygon3dList, tolXY, tolZ)

    #Give all corners an index that is used for optimization problem
    corners = []
    idx = 0
    for pol in polygon3dList:
        for cp in pol.cornerPoints:
            if cp not in corners:
                cp.updateIndex(idx)
                corners.append(cp)
                idx +=1
    
    #Calculate distance and flatness for each plane with no optimization
    flatnessNoOpt = []
    distanceNoOpt = []
    for pol in polygon3dList:
        f, d = calculateFlatnessDistance(pol.cornerPoints, pol)
        flatnessNoOpt.append(f)
        distanceNoOpt.append(d)
    
    res = optimization.opt(polygon3dList, corners)
    
    flatnessOpt = []
    distanceOpt = []
    for pol in polygon3dList:
        f, d = calculateFlatnessDistance(pol.cornerPoints, pol)
        flatnessOpt.append(f)
        distanceOpt.append(d)

    if sum(flatnessNoOpt)<sum(flatnessOpt): #If the building is less flat after optimization, we go back to original
        
        #Revert changes from optimization problem
        for idx, v in enumerate(corners):
            v.z-=res[idx] 
            v.z+=res[idx+1]
        
        #plotPoints(polygon3dList, ax)
        #plotPlanes(polygon3dList, ax)
        
        end = timeit.default_timer()
        return sum(flatnessNoOpt)/len(corners), sum(distanceNoOpt), end-start
    
    
    #plotPoints(polygon3dList, ax)
    #plotPlanes(polygon3dList, ax)
    end = timeit.default_timer()
    return sum(flatnessOpt)/len(corners), sum(distanceOpt), end-start


print(roofPlanes(150))
buildings = [18, 73, 87, 98, 106, 107, 108, 150, 163, 212, 279, 283, 317, 356, 380, 392, 406, 416, 424, 425, 434, 534, 556, 579, 618, 620]

gableBuildings = [73, 106, 108, 212, 392, 416, 425, 434]  

complexBuildings = [163, 356, 406, 618, 620]

hipBuildings = [18, 150, 317, 380, 554]

flatBuildings = [107, 283, 424, 534, 579]

#Runs roofplanes for a number of buildings and calculates each buildings flatness and fit to point
#Gamma and sigma are limits for when a building is considered correctly modeled or not
def result(buildings):
    correct = []
    wrong = []
    times = []
    flatness = []
    distance = []
    gamma = 0.4
    sigma = 0.05
    
    for building in buildings:
        f, d, t = roofPlanes(building)
        
        t2 = 0
        if f == False:
            f,d,t2= roofPlanes(building)
        t +=t2
        
        times.append(t)
        flatness.append(f)
        distance.append(d)
        if (f < sigma and d < gamma):
            correct.append(building)
        else:
            wrong.append(building)
            
    print(wrong)
    return len(correct), sum(flatness)/len(buildings), sum(distance)/len(buildings), sum(times)

#print(result(buildings))

