from geometry import Point2d
import json
footprints = [[] for i in range(0,630)]

filePath = './RoofPolygons.json'
f = open(filePath)
data = json.load(f)

roofpoints = data['Buildings'][0]['InnerPolygons'] # all roof points
roofpoints = [item for sublist in roofpoints for item in sublist]
startEnd = len(roofpoints)/2

pol1 = [Point2d(roofpoints[i]['x'], roofpoints[i]['y']) for i in range(5)]
pol2 = [Point2d(roofpoints[i]['x'], roofpoints[i]['y']) for i in range(5, len(roofpoints))]
polygons = [pol1, pol2]
footprints[159] = polygons

########################################
#MANUAL SEGMENTATIONS FROM HAMMARKULLEN#
########################################
#buildings = [18, 73, 98, 106, 107, 108, 150, 163, 212, 283, 317, 356, 380, 392, 406, 416, 424, 425, 434, 534, 556, 579, 618, 620]

# gableBuildings = [73, 98, 106, 108, 212, 392, 416, 425, 434]

# complex = [163, 356, 406, 618, 620]

# hipBuildings = [18, 87, 150, 317, 380, 554]

# flatBuildings = [107, 283, 424, 534, 579]


#Building 18, hip roof
pol1 = [Point2d(812.4, 907.5),
        Point2d(808, 901),
        Point2d(811.6, 893.9)]
pol2 = [Point2d(812.4, 907.5),
        Point2d(742.9, 911.4),
        Point2d(746, 904.8),
        Point2d(808, 901)]
pol3 = [Point2d(742.9, 911.4),
        Point2d(742.1, 897.7),
        Point2d(746, 904.8)]
pol4 = [Point2d(808, 901),
        Point2d(746, 904.8),
         Point2d(742.1, 897.7),
         Point2d(811.6, 893.9)]

polygons=[pol1,pol2,pol3,pol4]
footprints[18] = polygons




#Building 73, standard gable roof
pol1 = [Point2d(128.6, 89.3),
         Point2d(114.8, 90),
         Point2d(114.5, 83.7),
         Point2d(128, 82.6)]
pol2 = [Point2d(128, 82.6),
        Point2d(114.5, 83.7),
        Point2d(114.2, 77.4),
        Point2d(128, 76.65)]

polygons=[pol1, pol2]
footprints[73]=polygons

#Building 87, hip roof
pol1 = [Point2d(832.2, 892.9),
        Point2d(825.2, 891),
        Point2d(820.2, 800.2),
        Point2d(826.8, 795.7)]
pol2 = [Point2d(832.2, 892.9),
        Point2d(818.6, 893.7),
        Point2d(825.2, 891)]
pol3 = [Point2d(825.2, 891),
        Point2d(818.6, 893.7),
        Point2d(813.2, 796.4),
        Point2d(820.2, 800.2)]
pol4 = [Point2d(820.2, 800.2),
        Point2d(813.2, 796.4),
        Point2d(826.8, 795.7)]

polygons = [pol1,pol2,pol3,pol4]
footprints[87] = polygons

#Building 98, gable roof. Many corners
pol1 = [Point2d(968.851999962644,967.004995189607), 
        Point2d(966.340999962704,970.166995191015), 
        Point2d(967.135999962687,970.798995188437), 
        Point2d(964.611999962712,973.980995189399), 
        Point2d(960.931999962719, 977.020995188504), 
        Point2d(957.5, 974.3), 
        Point2d(965,964.2)]
pol2 = [Point2d(957.5, 974.3), 
        Point2d(953.698999962828,971.28699518647), 
        Point2d(961.628999962704, 961.280995190144), 
        Point2d(965,964.2)]
polygons = [pol1, pol2]

footprints[98] = polygons


#Building 106, gable roof
pol1 = [Point2d(128, 116), 
        Point2d(141, 116), 
        Point2d(141, 121), 
        Point2d(128, 121)]
pol2 = [Point2d(128, 116), 
        Point2d(128, 110), 
        Point2d(141, 110), 
        Point2d(141, 116)]
polygons = [pol1, pol2]

footprints[106] = polygons


#building 107, flat roof

pol1 = [Point2d(192.2, 677.5),
        Point2d(192.5, 683.8),
        Point2d(173.2, 684.8),
        Point2d(172.9, 678.5)]
polygons=[pol1]
footprints[107] = polygons



#building 108 big gable roof.
pol1 = [Point2d(464,260),
        Point2d(457.5, 260.5),
        Point2d(450, 122),
        Point2d(456.5, 122)]
pol2 = [Point2d(464,260),
        Point2d(456.5, 122),
        Point2d(462.4, 121.5),
        Point2d(470, 260)]
polygons=[pol1,pol2]
footprints[108]=polygons




# Building 150, large gable roof with many data points, and some outliers on
# the walls. Apparently its actually a hip roof. TODO change.
# pol1 = [Point2d(735.820999964606, 911.771995139308),
#         Point2d(666.344999965047, 915.551995122805),
#         Point2d(666, 909),
#         Point2d(735, 904.7),
#         Point2d(735.820999964606, 911.771995139308)]
# pol2 = [Point2d(665.598999965121, 901.818995122798),
#         Point2d(735.074999964447, 898.039995141327),
#         Point2d(735, 904.7),
#         Point2d(666, 909),
#         Point2d(665.598999965121, 901.818995122798)]
# polygons = [pol1, pol2]
#
# footprints[150] = polygons

# Building 150, hip roof with many data points and som outliers on the walls.
pol1 = [Point2d(666.344999965047, 915.551995122805),
        Point2d(665.598999965121, 901.818995122798),
        Point2d(670.5, 908.75),
        Point2d(666.344999965047, 915.551995122805)]
pol2 = [Point2d(666.344999965047, 915.551995122805),
        Point2d(670.5, 908.75),
        Point2d(731, 905.5),
        Point2d(735.820999964606, 911.771995139308),
        Point2d(666.344999965047, 915.551995122805)]
pol3 = [Point2d(670.5, 908.75),
        Point2d(665.598999965121, 901.818995122798),
        Point2d(735.074999964447, 898.039995141327),
        Point2d(731, 905.5),
        Point2d(670.5, 908.75)]
pol4 = [Point2d(735.074999964447, 898.039995141327),
        Point2d(735.820999964606, 911.771995139308),
        Point2d(731, 905.5),
        Point2d(735.074999964447, 898.039995141327)]
polygons = [pol1, pol2, pol3, pol4]
footprints[150] = polygons


#Building 163, gable roof in curvy buildings

pol1 = [Point2d(691.4, 54.7),
        Point2d(686.5, 53.5),
        Point2d(688.3, 48.2),
        Point2d(693.1, 49.9)]

pol2 = [Point2d(686.5, 53.5),
        Point2d(679.1, 51.4),
        Point2d(681.1, 45.7),
        Point2d(688.3, 48.2)]

polygons=[pol1, pol2]

footprints[163]=polygons

#Building 212, gable roof in one of the "donuts". Did it to plot together with 434 which is next to it.
pol1 = [Point2d(762.14, 195.31),
         Point2d(759.81, 200.45),
         Point2d(753.049999963492, 196.839995151386),
         Point2d(755.946999963373, 191.492995149456)]
pol2 = [Point2d(762.14, 195.31),
        Point2d(766.968999963487, 198.234995153733),
        Point2d(764.402999963379, 202.928995152004),
        Point2d(759.81, 200.45)]
polygons=[pol1, pol2]
footprints[212]=polygons

#Building 279, gable roof
pol1 = [Point2d(995.6, 832.8),
        Point2d(991.6, 835.4),
        Point2d(983.8, 822.8),
        Point2d(987.7, 820.4)]
pol2 = [Point2d(991.6, 835.4),
        Point2d(987.8, 837.8),
        Point2d(979.9, 825.3),
        Point2d(983.8, 822.8)]
polygons=[pol1,pol2]
footprints[279]=polygons




#Building 283, flat roof with hole. Very common in Hammarkullen
pol1 = [Point2d(81.3, 351.2),
        Point2d(100.5, 350.3),
        Point2d(100.8, 356.4),
        Point2d(81.6, 357.3)]
polygons=[pol1]
footprints[283]=polygons

# Building 317, hip roof
pol1 = [Point2d(975.37, 949.31),
        Point2d(977.03099996259, 943.219995191321),
        Point2d(985.620999962441, 954.15299519524),
        Point2d(979.50, 954.32)]
pol2 = [Point2d(979.50, 954.32),
        Point2d(985.620999962441, 954.15299519524),
        Point2d(978.461999962572, 959.775995193049)]
pol3 = [Point2d(979.50, 954.32),
        Point2d(978.461999962572, 959.775995193049),
        Point2d(969.872999962594, 948.843995191157),
        Point2d(975.37, 949.31)]
pol4 = [Point2d(975.37, 949.31),
        Point2d(969.872999962594, 948.843995191157),
        Point2d(977.03099996259, 943.219995191321)]
polygons = [pol1, pol2, pol3, pol4]
footprints[317] = polygons

#Building 356, complex gable roof with outsticker. Segmentation is a litte weird, should maybe move marked point
pol1=[Point2d(37.5, 190.8),
      Point2d(33.8, 189.6),
      Point2d(38.6, 174.1),
      Point2d(42.3, 175.2)] #Right plane of big gable

pol2=[Point2d(33.8, 189.6),
      Point2d(30.2, 188.5),
      Point2d(34.3, 172.8),
      Point2d(38.6, 174.1)]#Left plane of big gable

pol3=[Point2d(30.2, 188.5),
      Point2d(23.1, 186.4),
      Point2d(24.3, 182.6),
      Point2d(32.4, 185.4)]#Mark #Top of small gable

pol4 = [Point2d(32.4, 185.4),#Mark
        Point2d(24.3, 182.6),
        Point2d(25.3, 179.1),
        Point2d(31.6, 181.1)]#Bot of small gable

polygons=[pol1,pol2,pol3,pol4]
footprints[356]=polygons

#Building 380 hip,roof

pol1=[Point2d(775.8, 876.1),
      Point2d(762.1, 876.9),
      Point2d(768.7, 873.5)]
pol2= [Point2d(768.7, 873.5),
       Point2d(762.1, 876.9),
       Point2d(757.8, 799.5),
       Point2d(765.3, 802.3)]
pol3 = [Point2d(765.3, 802.3),
        Point2d(757.8, 799.5),
        Point2d(771.5, 798.8)]
pol4 = [Point2d(775.8, 876.1),
         Point2d(768.7, 873.5),
         Point2d(765.3, 802.3),
         Point2d(771.5, 789.8)]

polygons = [pol1,pol2,pol3,pol4]

footprints[380] = polygons

# Building 392, gable
pol1 = [Point2d(706, 880),
        Point2d(699, 880),
        Point2d(695.5, 802),
        Point2d(702, 802)]
pol2 = [Point2d(706, 880),
        Point2d(702, 802),
        Point2d(709, 802),
        Point2d(711.5, 880)]
polygons = [pol1, pol2]
footprints[392] = polygons


# Building 406, big gable roof with a turn. Works really well, can set tolZ as low as 0.4 and still get good results.
# Also, because the building is very large it needs adjusted z-axes for the
# plot to look good. Put
# ax.set_zlim3d(100, 120)
# before plt.show()
pol1 = [Point2d(660, 194.6),
        Point2d(656.2, 200.5),
        Point2d(597.8, 204),
        Point2d(597, 196.9),
        Point2d(660, 194.6)]
pol2 = [Point2d(660, 194.6),
        Point2d(597, 196.9),
        Point2d(597, 192),
        Point2d(662.8, 188.7),
        Point2d(660, 194.6)]
pol3 = [Point2d(660, 194.6),
        Point2d(662.8, 188.7),
        Point2d(688.9, 243.5),
        Point2d(683.9, 245.4),
        Point2d(660, 194.6)]
pol4 = [Point2d(660, 194.6),
        Point2d(683.9, 245.4),
        Point2d(677.8, 247.3),
        Point2d(656.2, 200.5),
        Point2d(660, 194.6)]
polygons = [pol1, pol2, pol3, pol4]
footprints[406] = polygons


# Building 416, gable roof with slightly fewer points
pol1 = [Point2d(858.9, 271.48),
        Point2d(853.55, 269.56),
        Point2d(854.60, 267.02),
        Point2d(860.05, 268.15),
        Point2d(858.9, 271.48)]
pol2 = [Point2d(858.9, 271.48),
        Point2d(860.05, 268.15),
        Point2d(865.97, 269.04),
        Point2d(864.99, 272.65),
        Point2d(858.9, 271.48)]
polygons = [pol1, pol2]
footprints[416] = polygons

#building 424, flat roof with some outliers
pol1= [Point2d(91.6, 462.2),
       Point2d(91.9, 468.4),
       Point2d(72.7, 469.5),
       Point2d(72.4, 463.3)]
polygons=[pol1]
footprints[424]=polygons


# Building 425
# Pretty hard to segment this roof but it generates buckled roofs, which is not perfect
pol1 = [Point2d(79.75, 858.29),
        Point2d(80.58, 864.53),
        Point2d(75.63, 866.25),
        Point2d(74.80, 860.30)]
pol2 = [Point2d(79.75, 858.29),
        Point2d(84.86, 856.76),
        Point2d(86.21, 862.52),
        Point2d(80.58, 864.53)]
polygons = [pol1, pol2]
footprints[425] = polygons

# Building 434, a gable roof in on of the "donuts". Did it to plot together with 212 which is next to it.
pol1 = [Point2d(759.41, 200.29),
         Point2d(757.18, 204.96),
         Point2d(750.196999963606, 202.133995149285),
         Point2d(753.049999963492, 196.839995151386)]
pol2 = [Point2d(759.41, 200.29),
        Point2d(764.402999963379, 202.928995152004),
        Point2d(762.195999963384, 206.969995149411),
        Point2d(757.18, 204.96)]
polygons = [pol1, pol2]
footprints[434] = polygons

# Building 534, double flat roof with different heights.
pol1 = [Point2d(692, 702.5),
        Point2d(696, 702.5),
        Point2d(696, 711),
        Point2d(692, 711),
        Point2d(692, 702.5)]
pol2 = [Point2d(692, 702.5),
        Point2d(692, 711),
        Point2d(684, 711),
        Point2d(684, 702.5),
        Point2d(692, 702.5)]
polygons = [pol1, pol2]
footprints[534] = polygons

#building 559, hip with one side tilted

pol1 = [Point2d(605.1, 487),
        Point2d(597.6, 487.2),
        Point2d(593.5, 406),
        Point2d(600.1, 397.8)]
pol2 = [Point2d(597.6, 487.2),
        Point2d(590.3, 487.9),
        Point2d(585.4, 398.6),
        Point2d(593.5, 406)]
pol3 = [Point2d(593.5, 406),
        Point2d(585.4, 398.6),
        Point2d(600.1, 397.8)]
polygons=[pol1,pol2,pol3]
footprints[559]=polygons


#building 556, quite unique building with a low gable roof and a higher half hip roof
pol1 = [Point2d(890, 544),
        Point2d(888, 518),
        Point2d(895, 517),
        Point2d(896, 544)] #lefft gable
pol2 = [Point2d(904, 543.5),
        Point2d(896, 544),
        Point2d(895, 517),
        Point2d(902, 518)] #right galbe
pol3 = [Point2d(888,517),
        Point2d(885,471.5),
        Point2d(893, 472),
        Point2d(895, 510)] #left hip
pol4 = [Point2d(888,517),
        Point2d(895, 510),
        Point2d(902.5, 515.5)] #small hip
pol5 = [Point2d(902.5, 515.5),
        Point2d(895, 510),
        Point2d(893, 472),
        Point2d(900, 479.7)] #right hip
polygons= [pol1, pol2, pol3, pol4, pol5]
footprints[556]=polygons


# Building 579, double flat roof with different heights.
pol1 = [Point2d(744.5, 708.07),
        Point2d(737, 708.07),
        Point2d(736.5, 698),
        Point2d(744.5, 698),
        Point2d(744.5, 708.07)]
pol2 = [Point2d(744.5, 708.07),
        Point2d(744.5, 698),
        Point2d(748.5, 698),
        Point2d(749, 707.5),
        Point2d(744.5,  708.07)]
polygons = [pol1, pol2]
footprints[579] = polygons



# Building 618, double gable roof. When plotted, it becomes visible that the
# segmentation is a little off.
pol1 =  [Point2d(849.3, 678),
         Point2d(849.3, 681.5),
         Point2d(836, 681.5),
         Point2d(836, 678),
         Point2d(849.3, 678)]
pol2  = [Point2d(849.3, 681.5),
         Point2d(849.3, 685),
         Point2d(836, 685),
         Point2d(836, 681.5),
         Point2d(849.3, 681.5)]
pol3  = [Point2d(849.3, 685),
         Point2d(849.3, 688),
         Point2d(836, 688),
         Point2d(836, 685),
         Point2d(849.3, 685)]
pol4  = [Point2d(849.3, 688),
         Point2d(849.3, 691.5),
         Point2d(836, 691.5),
         Point2d(836, 688),
         Point2d(849.3, 688)]
polygons = [pol1, pol2, pol3, pol4]

footprints[618] = polygons



# Building 620, large L-shaped gable roof. Needs the outlier filtering,
# otherwise pol1 will get a bad plane slope. 
# Also, because the building is very large it needs adjusted z-axes for the 
# plot to look good. Put 
# ax.set_zlim3d(100, 120)
# before plt.show()
pol1 = [Point2d(454.9, 79.8),
        Point2d(456.4, 121.9),
        Point2d(450, 122.2),
        Point2d(448, 73.7)]
pol2 = [Point2d(454.9, 79.8),
        Point2d(460.9, 84.7),
        Point2d(461.8, 121.5),
        Point2d(456.4, 121.9)]
pol3 = [Point2d(454.9, 79.8),
        Point2d(529.8, 75.4),
        Point2d(529.8, 81.1),
        Point2d(460.9, 84.7)]
pol4 = [Point2d(454.9, 79.8),
        Point2d(448, 73.7),
        Point2d(529.6, 69),
        Point2d(529.8, 75.4)]
polygons = [pol1, pol2, pol3, pol4]

footprints[620] = polygons
